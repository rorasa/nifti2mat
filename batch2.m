function varargout = batch2(varargin)
% BATCH MATLAB code for batch.fig
%      BATCH, by itself, creates a new BATCH or raises the existing
%      singleton*.
%
%      H = BATCH returns the handle to a new BATCH or the handle to
%      the existing singleton*.
%
%      BATCH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BATCH.M with the given input arguments.
%
%      BATCH('Property','Value',...) creates a new BATCH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before batch_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to batch_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help batch

% Last Modified by GUIDE v2.5 06-Jan-2012 18:24:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @batch_OpeningFcn, ...
                   'gui_OutputFcn',  @batch_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before batch is made visible.
function batch_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to batch (see VARARGIN)

% Choose default command line output for batch
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes batch wait for user response (see UIRESUME)
% uiwait(handles.figure1);

batchConversion(handles,varargin(2));

% --- Outputs from this function are returned to the command line.
function varargout = batch_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function batchConversion(handles,varin)

mainHandles = varin{1,1};

FileName = mainHandles.batchFileName;
PathName = mainHandles.batchPathName;
totalFile = size(FileName,2);
set(handles.totalText,'String',num2str(totalFile));

progressbar

for i = 1:totalFile
   curFileName = FileName{1,i};   
   imgStruct = load([PathName,curFileName]);
   img2 = abs(imgStruct.img);
   [x,y,z] = size(img2);
   img = img2(x:-1:1,y:-1:1,z:-1:1);
   voxsiz = [str2num(get(mainHandles.VoxelSizeXEdit,'String')),...
        str2num(get(mainHandles.VoxelSizeYEdit,'String')),...
        str2num(get(mainHandles.VoxelSizeZEdit,'String'))];
    datatype = str2num(get(mainHandles.DataTypeEdit,'String'));
    nii = make_nii(img, voxsiz, [], datatype);
    save_nii(nii,[PathName,curFileName(1:(size(curFileName,2)-4))]);

    [x,y,z] = size(img);
    img = img(x:-1:1,y:-1:1,z:-1:1);
    save([PathName,curFileName(1:(size(curFileName,2)-4))],'img');
   
   set(handles.doneText,'String',num2str(i));
   progressbar(i/totalFile);
end
set(handles.closeButton,'Enable','on');


% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1);
