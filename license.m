function varargout = license(varargin)
% LICENSE MATLAB code for license.fig
%      LICENSE, by itself, creates a new LICENSE or raises the existing
%      singleton*.
%
%      H = LICENSE returns the handle to a new LICENSE or the handle to
%      the existing singleton*.
%
%      LICENSE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LICENSE.M with the given input arguments.
%
%      LICENSE('Property','Value',...) creates a new LICENSE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before license_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to license_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help license

% Last Modified by GUIDE v2.5 21-Mar-2013 16:52:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @license_OpeningFcn, ...
                   'gui_OutputFcn',  @license_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before license is made visible.
function license_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to license (see VARARGIN)

% Choose default command line output for license
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes license wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = license_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function mainText_Callback(hObject, eventdata, handles)
% hObject    handle to mainText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of mainText as text
%        str2double(get(hObject,'String')) returns contents of mainText as a double


% --- Executes during object creation, after setting all properties.
function mainText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mainText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
text = sprintf(['NIfTI 2 MAT Converter\n',...
        'http://rorasa.wordpress.com/2013/03/21/nifti-2-mat-converter/ \n',...
        '2013(CC-BY-SA), Wattanit Hotrakool\n\n',...
        'This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.',...
        'To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.']);
set(hObject,'String',text);

% --- Executes on button press in toolboxLicense.
function toolboxLicense_Callback(hObject, eventdata, handles)
% hObject    handle to toolboxLicense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

text = sprintf(['Tools for NIfTI and ANALYZE image in MATLAB\n',...
        'http://research.baycrest.org/~jimmy/NIfTI/ \n',...
        'Copyright (c) 2009, Jimmy Shen\n',...
        'All rights reserved.\n\n',...
        'Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:\n',...
        '* Redistributions of source code must retain the above copyright notice, this   list of conditions and the following disclaimer.\n',...
        '* Redistributions in binary form must reproduce the above copyright notice, ',...
        '\n  this list of conditions and the following disclaimer in the documentation \n  and/or other materials provided with the distribution.\n',...
        '\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, ',...
        'INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.\n',...
        'IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, ',...
        'EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, ',...
        'STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, ',...
        'EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.']);
set(handles.mainText,'String',text);


% --- Executes on button press in myLicense.
function myLicense_Callback(hObject, eventdata, handles)
% hObject    handle to myLicense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

text = sprintf(['NIfTI 2 MAT Converter\n',...
        'http://rorasa.wordpress.com/2013/03/21/nifti-2-mat-converter/ \n',...
        '2013(CC-BY-SA), Wattanit Hotrakool\n\n',...
        'This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.',...
        'To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.']);
set(handles.mainText,'String',text);
