function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 21-Mar-2013 16:32:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Load library
addpath('NIFTI');

% Initialise custom setting
axis([handles.axes1,handles.axes2,handles.axes3],'off');

% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex] = uigetfile(...
    {'*.nii;*.hdr;*.img','NiFTI-1 File(*.nii,*.hdr,*.img)';...
     '*.nii','NifTI-1 Package(.nii)';...
     '*.hdr','NifTI-1 Header(.hdr)';...
     '*.img','NiFTI-1 Image(.img)';'*.*','All files'});
handles.inputPathName = PathName;
handles.inputFileName = FileName;
handles.inputContent = load_nii([PathName,FileName]);
guidata(hObject,handles);
Display_refresh(handles);
set(handles.DataTypeEdit,'String',handles.inputContent.hdr.dime.datatype);
pixdim = handles.inputContent.hdr.dime.pixdim;
set(handles.VoxelSizeXEdit,'String',pixdim(2));
set(handles.VoxelSizeYEdit,'String',pixdim(3));
set(handles.VoxelSizeZEdit,'String',pixdim(4));

% --- Update display panel.
function Display_refresh(handles)
img = handles.inputContent.img;
[x,y,z] = size(img);

% show Axial View
temp(:,:) = img(x:-1:1,y:-1:1,round(z/2));
imshow(temp',[],'Parent',handles.axes1);

% show Sagittal View
temp = [];
temp(:,:) = img(x:-1:1,round(y/2),z:-1:1);
imshow(temp',[],'Parent',handles.axes2);

% show Coronal View
temp = [];
temp(:,:) = img(round(x/2),y:-1:1,z:-1:1);
imshow(temp',[],'Parent',handles.axes3);

% --- Executes on button press in saveMatbutton.
function saveMatbutton_Callback(hObject, eventdata, handles)
% hObject    handle to saveMatbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PathName = handles.inputPathName;
FileName = handles.inputFileName;
img2 = handles.inputContent.img;
[x,y,z] = size(img2);
img = img2(x:-1:1,y:-1:1,z:-1:1);
save([PathName,FileName(1:(size(FileName,2)-4))],'img');

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ptFigure = get(handles.figure1,'CurrentPoint');
ptAxes1 = get(handles.axes1,'CurrentPoint');
ptAxes2 = get(handles.axes2,'CurrentPoint');
ptAxes3 = get(handles.axes3,'CurrentPoint');

if ptFigure(1,1) > 40.8 & ptFigure(1,1) < 100.8 & ptFigure(1,2) > 19.3 & ptFigure(1,2) < 36.3
    set(handles.mouseXText,'String',ptAxes1(1,1));
    set(handles.mouseYText,'String',ptAxes1(1,2));
    set(handles.mouseAxesText,'String','on Axial display');
elseif ptFigure(1,1) > 33.6 & ptFigure(1,1) < 68.6 & ptFigure(1,2) > 7.0 & ptFigure(1,2) < 18.5
    set(handles.mouseXText,'String',ptAxes2(1,1));
    set(handles.mouseYText,'String',ptAxes2(1,2));
    set(handles.mouseAxesText,'String','on Sagittal display');
elseif ptFigure(1,1) > 71.2 & ptFigure(1,1) < 107.6 & ptFigure(1,2) > 7.0 & ptFigure(1,2) < 18.5
    set(handles.mouseXText,'String',ptAxes3(1,1));
    set(handles.mouseYText,'String',ptAxes3(1,2));
    set(handles.mouseAxesText,'String','on Coronal display');
else
    set(handles.mouseXText,'String','x');
    set(handles.mouseYText,'String','y');
    set(handles.mouseAxesText,'String','Out of range');
end
% set(handles.mouseXText,'String',ptFigure(1,1));
% set(handles.mouseYText,'String',ptFigure(1,2));


% --- Executes on button press in batchButton.
function batchButton_Callback(hObject, eventdata, handles)
% hObject    handle to batchButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName,FilterIndex] = uigetfile(...
    {'*.nii','NifTI-1 Package(.nii)';...
     '*.hdr','NifTI-1 Header(.hdr)';...
     '*.img','NiFTI-1 Image(.img)';'*.*','All files'},...
     'MultiSelect','on');
 if ~iscell(FileName)
     FileName = {FileName};
 end
handles.batchPathName = PathName;
handles.batchFileName = FileName;
batch(hObject,handles);


% --- Executes on button press in batchButton2.
function batchButton2_Callback(hObject, eventdata, handles)
% hObject    handle to batchButton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex] = uigetfile(...
     {'*.mat','Matlab (.mat)';...
     '*.*','All files'},...
     'MultiSelect','on');
 if ~iscell(FileName)
     FileName = {FileName};
 end
handles.batchPathName = PathName;
handles.batchFileName = FileName;
batch2(hObject,handles);

% --- Executes on button press in loadMatButton.
function loadMatButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadMatButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex] = uigetfile(...
    {'*.mat','Matlab (.mat)';...
     '*.*','All files'});
handles.inputPathName = PathName;
handles.inputFileName = FileName;
imgStruct = load([PathName,FileName]);
img2 = imgStruct.img;
[x,y,z] = size(img2);
imgStruct.img = img2(x:-1:1,y:-1:1,z:-1:1);
handles.inputContent = imgStruct;
guidata(hObject,handles);
Display_refresh(handles);

% --- Executes on button press in saveNIFTIButton.
function saveNIFTIButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveNIFTIButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PathName = handles.inputPathName;
FileName = handles.inputFileName;
img2 = handles.inputContent.img;
voxsiz = [str2num(get(handles.VoxelSizeXEdit,'String')),...
    str2num(get(handles.VoxelSizeYEdit,'String')),...
    str2num(get(handles.VoxelSizeZEdit,'String'))];
datatype = str2num(get(handles.DataTypeEdit,'String'));
nii = make_nii(img2, voxsiz, [], datatype);
save_nii(nii,[PathName,FileName(1:(size(FileName,2)-4))]);

[x,y,z] = size(img2);
img = img2(x:-1:1,y:-1:1,z:-1:1);
save([PathName,FileName(1:(size(FileName,2)-4))],'img');

function DataTypeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to DataTypeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DataTypeEdit as text
%        str2double(get(hObject,'String')) returns contents of DataTypeEdit as a double


% --- Executes during object creation, after setting all properties.
function DataTypeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DataTypeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VoxelSizeXEdit_Callback(hObject, eventdata, handles)
% hObject    handle to VoxelSizeXEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VoxelSizeXEdit as text
%        str2double(get(hObject,'String')) returns contents of VoxelSizeXEdit as a double


% --- Executes during object creation, after setting all properties.
function VoxelSizeXEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VoxelSizeXEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VoxelSizeYEdit_Callback(hObject, eventdata, handles)
% hObject    handle to VoxelSizeYEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VoxelSizeYEdit as text
%        str2double(get(hObject,'String')) returns contents of VoxelSizeYEdit as a double


% --- Executes during object creation, after setting all properties.
function VoxelSizeYEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VoxelSizeYEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VoxelSizeZEdit_Callback(hObject, eventdata, handles)
% hObject    handle to VoxelSizeZEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VoxelSizeZEdit as text
%        str2double(get(hObject,'String')) returns contents of VoxelSizeZEdit as a double


% --- Executes during object creation, after setting all properties.
function VoxelSizeZEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VoxelSizeZEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over text10.
function text10_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to text10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
license;
%web('www.google.co.uk','-browser')
